import request from "supertest";

import app, { server } from "../index";

describe("index route", () => {
  afterAll(() => {
    server.close();
  });

  test("should respond with a 200 with no query parameters", () => {
    return request(app)
      .get("/")
      .expect("Content-Type", /html/)
      .expect(200)
      .then(response => {
        expect(response.text).toMatch(/Pizzima/);
      });
  });
});



app.get('/user', function(req, res) {
  res.status(200).json({ name: 'john' });
});


describe('GET /user', function() {
  it('respond with json', function(done) {
    request(app)
      .get('/user')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});
